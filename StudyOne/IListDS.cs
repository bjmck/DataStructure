﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudyOne
{
	public interface IListDS<T>
	{
		//求长度
		int GetLength();
		//清空操作
		void Clear();
		//判断线性表是否为空
		bool IsEmpty();
		//判断线性表是否为满
		bool IsFull();
		//添加操作
		void Append(T item);
		//插入操作
		void Insert(T item,int i);
		//删除操作
		T Delete(int i);
		//取表元
		T GetItem(int i);
		//按值查找
		int Locate(T value);
		//顺序倒置
		void Reverse();

	}
}
