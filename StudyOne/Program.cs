﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudyOne
{
	class Program
	{
		static void Main(string[] args)
		{
			SeqList<int> list = new SeqList<int>(8);
			list.Insert(11, 1);
			list.Insert(23, 2);
			list.Insert(36, 3);
			list.Insert(45, 4);
			list.Insert(80, 5);
			list.Insert(60, 6);
			list.Insert(40, 7);
			list.Insert(6, 8);
			for (int i = 0; i < list.GetLength(); i++)
			{
				Console.WriteLine(list.GetItem(i));
			}
			Console.ReadLine();
		}

		//顺序表倒置
		//效果:原来的顺序——11,23,36,45,80,60,40,6		倒置后:6,40,60,80,45,36,23,11
		//思路:把第一个元素与最后一个元素交换，把第二个元素与倒数第二个交换。一般地，把第i个元素与第n-i个元素交换，i的取值范围是0到n/2(n为顺序表的长度)
		static public void ReversSeqList(SeqList<int> L)
		{
			int tmp = 0;
			int len = L.GetLength()-1;
			for (int i = 0; i <= len/2; i++)
			{
				tmp = L[i];
				L[i] = L[len - i];
				L[len - i] = tmp;
			}
		}

		//两顺序表合并
		//问题：有数据类型为整型的顺序表La和Lb，其数据元素按从小到大的升序排列，编写一个算法将他们合并成一个表Lc，要求Lc中的数据元素也按升序排列。
		//思路：依次扫描La和Lb的数据元素，比较La和Lb当前的元素的值，将较小的值的数据元素赋值给Lc，如此直到一个顺序表被扫描完，然后将未完的那个顺序表中余下的数据元素赋给Lc即可。Lc的容量要能够容纳La和Lb两个表相加的长度。
		static public SeqList<int> Merge(SeqList<int> la,SeqList<int> lb )
		{
			SeqList<int> lc=new SeqList<int>(la.MaxSize + lb.MaxSize);
			int i = 0;
			int j = 0;
			int k = 0;
			//两个表中都有数据元素
			while((i <= (la.GetLength()-1))&&(j<=(lb.GetLength()-1)))
			{
				if(la[i]<lb[j])
				{
					lc.Append(la[i++]);
				}
				else
				{
					lc.Append(lb[j++]);
				}
			}

			//a表中还有数据元素
			while (i <= (la.GetLength()-1))
			{
				lc.Append(la[i++]);
			}

			//b表中还有数据元素
			while (j <=(lb.GetLength()-1))
			{
				lc.Append(lb[j++]);
			}
			return lc;
		}

		//两顺序表去同存异
		//问题：已知一个存储整数的顺序表La,试构造顺序表Lb,要求顺序表Lb中只包含顺序表La中所有值不相同的数据元素。
		//思路：先把顺序表La的第1个元素赋值给顺序表Lb,然后从顺序表La的第2个元素起，每一个元素与顺序表lb中每一个元素进行比较，如果不相同，则把该元素附加到顺序表Lb的末尾。
		static public SeqList<int> Pubge(SeqList<int> la)
		{
			SeqList<int> lb = new SeqList<int>(la.MaxSize);
			//将a表中的第1个数据元素赋给b表
			lb.Append(la[0]);
			//依次处理a表中的数据元素
			for (int i = 1; i <= la.GetLength(); i++)
			{
				int j = 0;
				//查看b表中有无与a表中相同的数据元素
				for (j = 0; j < lb.GetLength(); j++)
				{
					//有相同的元素
					if(la[i].CompareTo(lb[j])==0)
					{
						break;
					}
				}
				//没有相同的数据元素，将a表中的数据元素附加到b表的末尾
				if(j > lb.GetLength()-1)
				{
					lb.Append(la[i]);
				}
			}
			return lb;
		}
	}
}
