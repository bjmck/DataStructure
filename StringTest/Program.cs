﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StringTest
{
	class Program
	{
		static void Main(string[] args)
		{
			//string s1 = "foo";
			//StringBuilder sb = new StringBuilder("foo");
			//string s2 = sb.ToString();
			//bool flag=OpTest<string>(s1, s2);
			//Console.WriteLine(flag.ToString());
			//Console.ReadLine();

			string a1 = "abc";
			string a2 = "abc";
			StringBuilder sb = new StringBuilder("abc");
			string a3 = sb.ToString();
			//if(a1==a2)
			//    Console.WriteLine(a1+" == "+a2);
			//else
			//{
			//    Console.WriteLine(a1 + " != " + a2);
			//}
			//if(a1.Equals(a2))
			//    Console.WriteLine(a1 + " Equals " + a2);
			//else
			//{
			//    Console.WriteLine(a1 + " not Equals " + a2);
			//}

			//if (object.ReferenceEquals(a1,a2))
			//    Console.WriteLine(a1 + " ReferenceEquals " + a2);
			//else
			//{
			//    Console.WriteLine(a1 + " not ReferenceEquals " + a2);
			//}


			if (a1 == a3)
				Console.WriteLine(a1 + " == " + a3);
			else
			{
				Console.WriteLine(a1 + " != " + a3);
			}
			if (a1.Equals(a3))
				Console.WriteLine(a1 + " Equals " + a3);
			else
			{
				Console.WriteLine(a1 + " not Equals " + a3);
			}

			if (object.ReferenceEquals(a1, a3))
				Console.WriteLine(a1 + " ReferenceEquals " + a3);
			else
			{
				Console.WriteLine(a1 + " not ReferenceEquals " + a3);
			}
			Console.ReadLine();
		}

		public static bool OpTest<T>(T s, T t) where T : class,IComparable
		{
			if (s.Equals(t))
				return true;
			else
			{
				return false;
			}

		}
	}
}
