﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SequenceSearch
{
	/// <summary>
	/// 顺序查找时间复杂度：O(n);
	/// </summary>
	class Program
	{
		static void Main(string[] args)
		{
			List<int> list=new List<int>(){2,3,5,8,7};
			var result = SequenceSearch(list, 3);
			if(result !=-1)
				Console.WriteLine("3 已经在数组中找到，索引位置为:"+result);
			else
				Console.WriteLine("没有找到");
			Console.ReadLine();
		}

		/// <summary>
		/// 顺序查找
		/// </summary>
		/// <param name="list"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public static  int SequenceSearch(List<int> list,int key)
		{
			for (int i = 0; i < list.Count; i++)
			{
				//查找成功，返回序列号
				if(key==list[i])
					return i;
			}
			//查找不成功，返回-1
			return -1;
		}
	}
}
