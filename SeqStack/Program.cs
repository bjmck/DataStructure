﻿using System;

namespace SeqStack
{
	public class Program
	{
		static void Main(string[] args)
		{
			SeqStackClass stackManager=new SeqStackClass();
			SeqStack<Student> seqStack = stackManager.SeqStackInit<Student>(10);
			Console.WriteLine("********************  压入Id=1,Id=2,Id=3的元素  ***********************\n");
			//压入Id=1,Id=2,Id=3的元素
            stackManager.SeqStackPush(seqStack, new Student() { Id = 1, Name = "一线码农", Age = 23 });
            stackManager.SeqStackPush(seqStack, new Student() { Id = 2, Name = "huangxincheng520", Age = 23 });
            stackManager.SeqStackPush(seqStack, new Student() { Id = 3, Name = "51cto", Age = 23 });
			Console.WriteLine(".... 压入成功，当前栈中元素有：" + stackManager.SeqStackLen(seqStack) + "个");
			Console.WriteLine("\n******************  查看栈顶元素  ********************");
			var result = stackManager.SeqStackPeek(seqStack);
			Console.WriteLine("栈顶元素为：Id=" + result.Id + ",Name=" + result.Name + ",Age=" + result.Age);
			Console.WriteLine("\n********************  弹出栈顶元素  ***********************");
			stackManager.SeqStackPop(seqStack);
			Console.WriteLine("\n******************  查看栈中的元素  ********************");
			for (int i = 0; i < stackManager.SeqStackLen(seqStack); i++)
			{
					Console.WriteLine("栈顶元素为：Id=" + seqStack.data[i].Id + ",Name=" + seqStack.data[i].Name + ",Age=" + seqStack.data[i].Age);
			}
			Console.Read();
		}
	}

	#region 学生数据实体

	public class Student
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int Age { get; set; }
	}

	#endregion

	#region 栈的数据结构

	public class SeqStack<T>
	{
		public T[] data;
		//栈顶指针
		public int top = -1;
		public SeqStack(int length)
		{
			data=new T[length];
		}
	}

	#endregion

	#region 栈的基本操作

	public class SeqStackClass
	{
		#region 栈的初始化

		public SeqStack<T> SeqStackInit<T>(int length)
		{
			SeqStack<T> seqStack = new SeqStack<T>(length);
			seqStack.top = -1;
			return seqStack;
		}

		#endregion

		#region 判断栈是否为空

		public bool SeqStackIsEmpty<T>(SeqStack<T> seqStack)
		{
			return seqStack.top == -1;
		}

		#endregion

		#region 清空栈

		public void SeqStackClear<T>(SeqStack<T> seqStack)
		{
			 seqStack.top = -1;
		}

		#endregion

		#region 判断栈是否已满

		public bool SeqStackIsFull<T>(SeqStack<T> seqStack)
		{
			return seqStack.top == seqStack.data.Length;
		}

		#endregion

		#region 入栈

		public void SeqStackPush<T>(SeqStack<T> seqStack,T data)
		{
			if(SeqStackIsFull(seqStack))
				throw new Exception("栈溢出");
			seqStack.data[++seqStack.top] = data;
		}

		#endregion

		#region 出栈

		public T SeqStackPop<T>(SeqStack<T> seqStack)
		{
			if (SeqStackIsEmpty(seqStack))
				throw new Exception("栈为空");
			seqStack.data[seqStack.top] = default(T);
			return seqStack.data[--seqStack.top];
		}

		#endregion

		#region 获取栈顶元素

		public T SeqStackPeek<T>(SeqStack<T> seqStack)
		{
			if (SeqStackIsEmpty(seqStack))
				throw new Exception("栈为空");
			return seqStack.data[seqStack.top];
		}

		#endregion

		#region 获取栈中元素个数

        public int SeqStackLen<T>(SeqStack<T> seqStack)
        {
            return seqStack.top + 1;
        }

        #endregion

	}


	#endregion
}
