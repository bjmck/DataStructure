﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlList
{
	public class Program
	{
		static void Main(string[] args)
		{
			SelList seq = new SelList();
			SeqListType<Student> list=new SeqListType<Student>();
			Console.WriteLine("\n********************** 添加二条数据 ************************\n");
			seq.SeqListAdd<Student>(list, new Student() { Id = "1", Name = "夏荣安", Age = 22 });
			seq.SeqListAdd<Student>(list, new Student() { Id = "3", Name = "小然", Age = 23 });
			Console.WriteLine("添加成功");
			//展示数据
            Display(list);
			Console.WriteLine("\n********************** 正在搜索Name=“夏荣安”的实体 ************************\n");
			var student = seq.SeqListFindByKey<Student, string>(list, "夏荣安", s => s.Name);
            Console.WriteLine("\n********************** 展示一下数据 ************************\n");
            if (student != null)
                Console.WriteLine("ID:" + student.Id + ",Name:" + student.Name + ",Age:" + student.Age);
            else
                Console.WriteLine("对不起，数据未能检索到。");
            Console.WriteLine("\n********************** 插入一条数据 ************************\n");
            seq.SeqListInsert(list, 1, new Student() { Id = "2", Name = "博客园", Age = 40 });
            Console.WriteLine("插入成功");
            //展示一下
            Display(list);
            Console.WriteLine("\n********************** 删除一条数据 ************************\n");
            seq.SeqListDelete(list, 0);
            Console.WriteLine("删除成功");
            //展示一下数据
            Display(list);
            Console.Read();
		} 

		/// <summary>
		/// 显示输出结果
		/// </summary>
		/// <param name="list"></param>
		static void Display(SeqListType<Student> list )
		{
			Console.WriteLine("\n********************** 展示一下数据 ************************\n");
             if (list == null || list.ListLen == 0)
             {
                 Console.WriteLine("呜呜，没有数据");
                 return;
             }
             for (int i = 0; i < list.ListLen; i++)
             {
                 Console.WriteLine("Id:" + list.ListData[i].Id + ",Name:" + list.ListData[i].Name + ",Age:" + list.ListData[i].Age);
             }
		}
	}

	#region 学生类

	/// <summary>
	/// 学生类
	/// </summary>
	public class Student
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public int Age { get; set; }
	}

	#endregion

	#region 定义一个顺序表的存储结构

	/// <summary>
	/// 定义一个顺序表的存储结构
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class SeqListType<T>
	{
		private const int maxSize = 100;
		public int MaxSize
		{
			get { return maxSize; }
		}
		//数据为100个存储空间
		public T[] ListData=new T[maxSize];
		public int ListLen { get; set; }
	}

	#endregion

	#region 顺序表的相关操作

	/// <summary>
	/// 顺序表的相关操作
	/// </summary>
	public class SelList
	{
		#region 顺序表初始化

		/// <summary>
		/// 顺序表初始化
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="t"></param>
		public void SeqListInit<T>(SeqListType<T> t)
		{
			t.ListLen = 0;
		}

		#endregion

		#region 顺序表长度

		/// <summary>
		/// 顺序表长度
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="t"></param>
		public int SeqListLen<T>(SeqListType<T> t)
		{
			return t.ListLen;
		}

		#endregion

		#region 顺序表添加

		/// <summary>
		/// 顺序表添加
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="t"></param>
		public bool SeqListAdd<T>(SeqListType<T> t,T data)
		{
			//防止数组溢出
			if (t.ListLen == t.MaxSize)
				return false;
			t.ListData[t.ListLen++] = data;
			return true;
		}

		#endregion

		#region 顺序表的插入操作

		/// <summary>
		/// 顺序表的插入操作
		/// </summary>
		public bool SeqListInsert<T>(SeqListType<T> t,int n,T data)
		{
			//首先判断n是否合法
			if (n < 0 || n > t.MaxSize - 1)
				return false;
			//说明数组已满，不能进行插入操作
			if (t.ListLen==t.MaxSize)
			{
				return false;
			}
			//需要将插入点的数组依次向后移动
			for (int i = t.ListLen-1; i >=n; i--)
			{
				t.ListData[i + 1] = t.ListData[i];
			}
			//最后将data插入到腾出的位置中去
			t.ListData[n] = data;
			t.ListLen++;
			return true;
		}

		#endregion

		#region 顺序表的删除操作

		/// <summary>
		/// 顺序表的删除操作
		/// </summary>
		public bool SeqListDelete<T>(SeqListType<T> t,int n)
		{
			//判断删除位置是否非法
			if (n < 0 || n > t.ListLen - 1)
				return false;
			//说明数组已满
			if (t.ListLen == t.MaxSize)
			{
				return false;
			}
			//将n处后的元素向前移动
			for (int i = n; i < t.ListLen; i++)
			{
				t.ListData[i] = t.ListData[i+1];
			}
			//去掉数组最后一个元素
			--t.ListLen;
			return true;
		}

		#endregion

		#region 顺序表的关键字查找

		/// <summary>
		/// 顺序表的关键字查找
		/// </summary>
		public T SeqListFindByKey<T,W>(SeqListType<T> t,string key,Func<T,W> where) where W:IComparable
		{
			for (int i = 0; i < t.ListLen; i++)
			{
				if(where(t.ListData[i]).CompareTo(key)==0)
				{
					return t.ListData[i];
				}
			}
			return default(T);
		}

		#endregion

		#region 顺序表的按序号查找

		/// <summary>
		/// 顺序表的按序号查找
		/// </summary>
		public T SeqListFindByNum<T>(SeqListType<T> t, int n)
		{
			if (n < 0 || n > t.ListLen - 1)
				return default(T);
			return t.ListData[n];
		}

		#endregion
	}


	#endregion
}
